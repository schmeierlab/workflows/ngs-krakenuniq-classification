"""
Snakemake wrapper for Krakenuniq
"""

__author__ = "Sebastian Schmeier"
__copyright__ = "Copyright 2018, Sebastian Schmeier"
__email__ = "s.schmeier@gmail.com"
__license__ = "MIT"

from snakemake.shell import shell

extra = snakemake.params.get("extra", "")
log = snakemake.log_fmt_shell(stdout=False, stderr=True)
threads = snakemake.threads
db = snakemake.params.get("db")

output_out = snakemake.output.get("out")
output_report = snakemake.output.get("report")

sample = snakemake.input.get("sample")
r1 = sample[0]
assert r1 is not None, "input-> r1 is a required input parameter"

if len(sample) == 2: ## paired end
    paired = '--paired'
    r2 = sample[1]
    reads = '{} {}'.format(r1, r2)
else: ## single end
    paired = ''
    reads = '{}'.format(r1)

zip_ext =  r1.split('.')[-1]
zip_ext2 = r1.split('.')[-2]
fq = '--fasta-input'
if zip_ext == 'bz2':
    compression = '--bzip2-compressed'
    if zip_ext2 in ["fastq", "fq"]:
        fq = '--fastq-input'
elif zip_ext == 'gz':
    compression = '--gzip-compressed'
    if zip_ext2 in ["fastq", "fq"]:
        fq = '--fastq-input'
else:
    compression = ''
    if zip_ext in ["fastq", "fq"]:
        fq = '--fastq-input'
    
    
shell("krakenuniq {extra} --threads {threads} {compression} {paired} {fq} "
      "--report-file {output_report} --db {db} {reads} {log} | gzip > {output_out} ")
