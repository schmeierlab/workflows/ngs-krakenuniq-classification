log <- file(snakemake@log[[1]], open="wt")
sink(log)
sink(log, type="message")

library("DESeq2")

parallel <- FALSE
if (snakemake@threads > 1) {
    library("BiocParallel")
    # setup parallelization
    register(MulticoreParam(snakemake@threads))
    parallel <- TRUE
}

# colData and countData must have the same sample order, but this is ensured
# by the way we create the count matrix

cts <- read.delim(snakemake@input[["counts"]], header=TRUE, row.names="taxid", check.names = F)
cts["name"] <- NULL

#coldata <- read.table(snakemake@input[["samples"]], header=TRUE, row.names="sample")
cols <- colnames(cts)
coldata <- data.frame(cols)

dds <- DESeqDataSetFromMatrix(countData = cts, colData = coldata, design = ~ 1)

# normalization and preprocessing
dds <- DESeq(dds, parallel=parallel)

disp <- dispersionFunction(dds)
saveRDS(disp, file=snakemake@output[[2]])
# for new data
# use:
#disp<-readRDS(file=snakemake@output[[2]])
#ddsNew <- estimateSizeFactors(ddsNew)
#dispersionFunction(ddsNew) <- disp
#vsdNew <- varianceStabilizingTransformation(ddsNew, blind=FALSE) ## false important here to use the already calcualted dispersion

# obtain normalized counts
# do not use info of groups/conditons here as it would introduce a bias
cts <- varianceStabilizingTransformation(dds, blind=TRUE)
# print table of normalized cts
cts <- assay(cts)

gz <- gzfile(snakemake@output[[1]], "w")
header <- t(c("taxid", as.character(colnames(cts))))
write.table(header, file=gz, row.names=FALSE, col.names=FALSE, append=FALSE,  quote=FALSE, sep="\t")
write.table(cts, file=gz, append=FALSE, quote=FALSE, sep="\t", row.names=TRUE, col.names=FALSE)
close(gz)



