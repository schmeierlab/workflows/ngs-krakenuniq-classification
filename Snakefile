## WWORKFLOW PROJECT: Krakenuniq classification workflow
## INIT DATE: 2018
import pandas as pd
import glob, os, os.path, datetime, sys, csv
from os.path import join, abspath
from snakemake.utils import validate, min_version

##### set minimum snakemake version #####
min_version("5.1.2")

## =============================================================================
## SETUP
## =============================================================================

## define global Singularity image for reproducibility
## USE: "--use-singularity --use-conda" to run all jobs in container
singularity: "docker://continuumio/miniconda3:4.5.11"

## =======================================================================
## SET SOME DEFAULT PATH
SCHEMAS = abspath("schemas")
SCRIPTS = abspath("scripts")
WRAPPER = abspath("wrapper")
ENVS    = abspath("envs")

## =======================================================================
## load config and sample sheets #####
# can be overridden with --configfile
# validate config file
configfile: "config.yaml"
validate(config, schema=join(SCHEMAS, "config.schema.yaml"))

THREADS      = config["threads"]
BASEDIR      = abspath(config["basedir"])
RES          = join(BASEDIR, "results")
LOGDIR       = join(BASEDIR, "logs")
BENCHMARKDIR = join(BASEDIR, "benchmarks")

# TOOLS
# Krakenuniq
KRAKEN_DB    = config["krakenuniq"]["db"]
KRAKEN_EXTRA = config["krakenuniq"]["extra"]

KRONA_TAXDB  = config["krona"]["tax"]
KRONA_EXTRA  = config["krona"]["extra"]

## project specific outs
KRAKENDIR    = join(RES, "01_krakenuniq")
MATRIXDIR    = join(RES, "02_matrix")
MATRIXDIR2   = join(RES, "03_matrix_pct")
KRONADIR     = join(RES, "04_krona_per_sample")
KRONADIR2    = join(RES, "05_krona_per_condition")

LEVELS = [level.strip() for level in config["level"].split(',')]
for level in LEVELS:
    if level not in ["phylum","class","order","family","genus","species"]:
        sys.stderr.write('Level "{}" from configfile is invalid. Needs to be one of "phylum","class","order","family","genus","species"\n'.format(level))
        sys.exit()

# SAMPLES
## sample inputs
SAMPLESHEET  = abspath(config["samples"])

# validate samplesheet
samples = pd.read_table(SAMPLESHEET).set_index("sample", drop=False)
validate(samples, schema=join(SCHEMAS, "samples.schema.yaml"))

## =============================================================================
## LOAD SAMPLES, check if files exist
sys.stderr.write('Reading samples from samplesheet: "{}" ...\n'.format(SAMPLESHEET))

# test if sample in dir
for fname in samples["r1"]:
    if not os.path.isfile(fname):
        sys.stderr.write("File '{}' from samplesheet can not be found. Make sure the file exists. Exit\n".format(fname))
        sys.exit()

for fname in samples["r2"]:
    if type(fname) == str:
        if not os.path.isfile(fname):
            sys.stderr.write("File '{}' from samplesheet can not be found. Make sure the file exists. Exit\n".format(fname))
            sys.exit()


NUM_SAMPLES = len(samples["sample"])
sys.stderr.write('{} samples to process\n'.format(NUM_SAMPLES))

SAMPLES = list(samples["sample"])
CONDITIONS = set(samples["condition"])

## =============================================================================
## FUNCTIONS
## =============================================================================
def get_fasta(wildcards):
    # does not return fq2 if it is not present
    return samples.loc[(wildcards.sample), ["r1", "r2"]].dropna()


## get samples belonging to a condition
def get_samples_per_condition(wildcards):
    temp_s = list(samples[samples["condition"] == wildcards.condition]["sample"].unique())
    temp_s.sort()
    res = [join(KRAKENDIR, "%s/classified.txt"%(s)) for s in temp_s]
    return res 

## =============================================================================
## RULES
## =============================================================================

## Pseudo-rule to stae the final targets, so that the whole workflow is run.
rule all:
    input:
        [expand(join(MATRIXDIR2, "all.{level}.pct.txt.gz"), level = LEVELS),
         join(KRONADIR,"krona.html"),
         expand(join(KRONADIR2,"krona.{condition}.html"), condition = CONDITIONS)]

        
rule krakenuniq:
    input:
        sample = get_fasta
    output:
        out = join(KRAKENDIR, "{sample}/krakenuniq.txt.gz"),
        report = join(KRAKENDIR, "{sample}/krakenuniq.report.txt.gz")
    log:
        join(LOGDIR, "krakenuniq/{sample}.log")
    benchmark:
        join(BENCHMARKDIR, "krakenuniq/{sample}.txt")
    threads: THREADS
    conda:
        join(ENVS, "krakenuniq.yaml")
    params:
        extra = KRAKEN_EXTRA,
        db = KRAKEN_DB
    wrapper:
        "file://wrapper/krakenuniq/wrapper.py"
          

rule matrix:
    input:
        expand(join(KRAKENDIR, "{sample}/krakenuniq.report.txt.gz"), sample=SAMPLES)
    output:
        join(MATRIXDIR, "all.{level}.txt.gz")
    log:
        join(LOGDIR, "matrix_{level}.log")
    benchmark:
        join(BENCHMARKDIR, "matrix_{level}.txt")
    params:
        script = join(SCRIPTS, "build_matrix.py"),
        level = "{level}"
    shell:
        "python {params.script} {params.level} {input} 2> {log} | gzip > {output}" 
        

rule matrix_pct:
    input:
        join(MATRIXDIR, "all.{level}.txt.gz")
    output:
        join(MATRIXDIR2, "all.{level}.pct.txt.gz")
    log:
        join(LOGDIR, "matrix_pct/{level}.log")
    benchmark:
        join(BENCHMARKDIR, "matrix_pct/{level}.txt")
    conda:
        join(ENVS, "pandas.yaml")
    params:
        script = join(SCRIPTS, "matrix_pct.py"),
        extra = r'--header --index "1,2"'
    shell:
        "python {params.script} {params.extra} {input} 2> {log} | gzip > {output}"     


rule get_classfied:
    input:
        join(KRAKENDIR, "{sample}/krakenuniq.txt.gz")
    output:
        temp(join(KRAKENDIR, "{sample}/classified.txt"))
    shell:
        "zcat {input} | egrep '^C' > {output}"


rule krona_per_sample:
    input:
        files = expand(join(KRAKENDIR, "{sample}/classified.txt"), sample=SAMPLES)
    output:
        join(KRONADIR,"krona.html")
    log:
        out = join(LOGDIR, "krona_per_sample.out"),
        err = join(LOGDIR, "krona_per_sample.err")
    benchmark:
        join(BENCHMARKDIR, "krona_per_sample.txt")
    conda:
        join(ENVS, "krona.yaml")
    params:
        extra = KRONA_EXTRA,
        db = KRONA_TAXDB
    shell:
        "nice ktImportTaxonomy {params.extra} -tax {params.db} -q 2 -t 3 {input.files} -o {output} > {log.out} 2> {log.err}"
        
        
rule get_per_condition:
    input:
        files = get_samples_per_condition
    output:
        temp(join(KRAKENDIR, "condition.{condition}.txt"))
    shell:
        "cat {input.files} > {output}"


rule krona_per_condition:
    input:
        files = expand(join(KRAKENDIR, "condition.{condition}.txt"), condition=CONDITIONS)
    output:
        join(KRONADIR2,"krona.{condition}.html")
    log:
        out = join(LOGDIR, "krona_per_condition/{condition}.out"),
        err = join(LOGDIR, "krona_per_condition/{condition}.err")
    benchmark:
        join(BENCHMARKDIR, "krona_per_condition/{condition}.txt")
    conda:
        join(ENVS, "krona.yaml")
    params:
        extra = KRONA_EXTRA,
        db = KRONA_TAXDB
    shell:
        "nice ktImportTaxonomy {params.extra} -tax {params.db} -c -q 2 -t 3 {input.files} -o {output} > {log.out} 2> {log.err}"

         
rule clean:
    shell:
        "rm -rf {BASEDIR}/*"

