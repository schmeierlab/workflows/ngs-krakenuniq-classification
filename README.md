# PROJECT: KrakenUniq classification workflow

- AUTHOR: Sebastian Schmeier (s.schmeier@protonmail.com)
- DATE: 2018 


## OVEVIEW

- Classify reads from either fastq- or fasta-files with KrakenUniq ([https://github.com/fbreitwieser/krakenuniq](https://github.com/fbreitwieser/krakenuniq))
- Summarise the counts in a matrix form for all samples per level.
- Calculate a matrix of pct for each sample.
- Produce a krona plot for each sample.
- If different conditions are present in the samplesheet, produce a Krona plot for each condition.


## INSTALL WORKFLOW


```bash
# Install miniconda
# LINUX:
curl -O https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
# MACOSX:
curl -O https://repo.continuum.io/miniconda/Miniconda3-latest-MacOSX-x86_64.sh
bash Miniconda3-latest-MacOSX-x86_64.sh

# Add conda dir to PATH
echo 'export PATH="~/miniconda3/bin:$PATH"' >> ~/.bashrc
echo 'export PATH="~/miniconda3/bin:$PATH"' >> ~/.zshrc

# Make env
# this environment contains the bare base of what is required to run snakemake
conda env create --name snakemake --file envs/snakemake.yaml
conda activate snakemake
```


## INSTALL TEST DATA (optional)

Download test data set with:

```bash
git clone https://gitlab.com/schmeierlab/workflows/ngs-test-data.git
```


## BUILD A KRAKENUNIQ DATABASE 

```bash
# install krakenuniq
conda create -n krakenuniq --yes krakenuniq
conda activate krakenuniq

# Download seqs
krakenuniq-download --db bacteria --threads 12 --rsync --verbose --dust refseq/bacteria/Complete_Genome

# Build db
krakenuniq-build --db bacteria --kmer-len 31 --threads 12
```


## UPDATE KRONA TAXONOMY

Krona is a bit difficult sometimes.
We provide a Krona taxonomy file in `data/taxonomy`.
However, if you wish to update this (which might be a good idea), use this:

```bash
conda env create -n krona -f envs/krona.yaml
conda activate krona
bzip2 -d data/taxonomy/taxonomy.tab.bz2
ktUpdateTaxonomy.sh ~/data/taxonomy
conda deactivate
```


## RUN SNAKEMAKE WORKFLOW

Change the `config.yaml` to suit your data.

```bash
# Do a dryrun of the workflow, show rules, order, and commands
snakemake -np --use-conda --cores 10

# If necessary bind more folders for singularity outside of your home
snakemake -p --use-conda --use-singularity --cores 10 --singularity-args "--bind /mnt/DATA" 2> run.log

# show a detailed summary of the produced files and used commands
snakemake -D

# To delete all created files use
snakemake -p clean
```


## Information

### Bracken
Currently not used with KrakenUuniq.



## TODO

- None comes to mind
